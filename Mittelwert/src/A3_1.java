import java.util.Scanner;

public class A3_1 {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // ===========================
	  Scanner scan = new Scanner(System.in);
	  System.out.print("Geben Sie eine Zahl ein");
      double x = scan.nextDouble();
	  System.out.print("Geben Sie eine Zahl ein");
      double y = scan.nextDouble();
      double m;
      
     
      m = mittelwert(x,y);
      
      ausgabe(x, y, m);
   }
   public static void ausgabe(double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double mittelwert(double x, double y) {
	  
	   return (x + y) / 2.0;
   }
}