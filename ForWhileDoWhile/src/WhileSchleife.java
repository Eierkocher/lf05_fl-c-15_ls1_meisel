import java.util.Scanner;
public class WhileSchleife {

	public static void main(String[] args) {
		
		int i;
		int a;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		a = tastatur.nextInt();
		
		System.out.println(" 1) hochz�hlen");
		i = 0;
		while (i <= a) {
			System.out.println(i);
			i++;
		}
		
		System.out.println(" 2) runterz�hlen");
		i = a;
		while (i >= 0) {
			System.out.println(i);
			i--;
		}
	}
}
				