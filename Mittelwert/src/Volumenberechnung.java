public class Volumenberechnung {
	public static void main(String[] args) {
		System.out.printf("W�rfel: %.2f\n", wuerfel(5));
		System.out.printf("Quader: %.2f\n", quader(4, 3, 6));
		System.out.printf("Pyramide: %.2f\n", pyramide(3, 8));
		System.out.printf("Kugel: %.2f\n", kugel(3));
	}
	
	
	public static double wuerfel(double a) {
		return a*a*a;
	}
	
	public static double quader(double b, double c, double d) {
		return b*c*d;
	}
	
	public static double pyramide(double e, double f) {
		return e*e*(f/3);
	}
	
	public static double kugel(double g) {
		
		double v = (4/3)*(g*g*g)*Math.PI;
		return v;
	}
}
