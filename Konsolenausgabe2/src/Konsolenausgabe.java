//A 2.2 AB
//Teil 1
import java.util.Scanner; // Import der Klasse Scanner

public class Konsolenausgabe {
	

	public static void main(String[] args) // Hier startet das Programm
	{
		// Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl1 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl2 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis_plus = zahl1 + zahl2;
	    int ergebnis_minus = zahl1 - zahl2;
	    int ergebnis_mal = zahl1 * zahl2;
	    int ergebnis_geteilt = zahl1 / zahl2;
	     
	    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis_plus);   
	 
	    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
	    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis_minus);   
	 
	    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
	    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis_mal);   
	 
	    System.out.print("\n\n\nErgebnis der Division lautet: "); 
	    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis_geteilt);   
	 	    
	    
	    myScanner.close(); 
	     
	  }    
	
	}
