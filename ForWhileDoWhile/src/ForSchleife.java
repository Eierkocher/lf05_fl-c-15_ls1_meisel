import java.util.Scanner;
public class ForSchleife {

	public static void main(String[] args) {
		
		int i;
		int a;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		a = tastatur.nextInt();
				
		for (i = 0; i <= a; i++){
			System.out.println(i);
		}
		
		for (i = 0; i >= a; i--) {
			System.out.println(i);
		}
	}

}
