import java.util.Scanner;

public class Fahrtzeitausgabe {
	
	public static void main(String[] args) {
		int fahrzeit = 0;
		char haltInSpandau = 'n'; 
		char richtungHamburg = 'n';
		char haltInStendal = 'j';
		char endetIn = 'h';
		String endhalteStelle;
		
		Scanner tastatur = new scanner(System.in);
		
		fahrzeit = fahrzeit + 8; //Fahrzeit: Berlin Hbf -> Spandau
		
		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; //Halt in Spandau
		}
		
		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96; //Richtung Hamburg
			endhalteStelle = "Hamburg";
		} else {
			fahrzeit = fahrzeit + 34; //Richtung Hannover
			
			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16; //Halt in Stendal
			}
			else {
				fahrzeit = fahrzeit + 6; //Stendal umfahren
			}
			if (endetIn == 'w') {
				fahrzeit = fahrzeit + 29; //Endhalt Wolfsburg
				endhalteStelle = "Wolfsburg";
			}
			else if (endetIn == 'b') {
				fahrzeit = fahrzeit + 50; //Endhalt Braunschweig
				endhalteStelle = "Braunschweig";
			}
			else {
				fahrzeit = fahrzeit + 62; //Endhalt Hannover
				endhalteStelle = "Hannover";
			}
		}
		
		System.out.println("Sie erreichen " + endhalteStelle + " in " + fahrzeit + " Minuten.");
		
	}

}
